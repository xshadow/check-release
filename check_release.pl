#! /usr/bin/perl -w

# check_release.pl: monitor manually installed software for new upstream
#                   releases.
#
# Copyright 2012-2017 Jonas Meurer <jonas@freesources.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Usage examples:
#
# check_release.pl --name=roundcube --changelog=/var/www/roundcube/CHANGELOG \
#         --url='https://github.com/roundcube/roundcubemail/releases' \
#         --regex='roundcubemail/releases/tag/([^v]__VER__)'
#
# check_release.pl --name=linux --changelog=/usr/src/linux/ChangeLog \
#         --url='https://www.kernel.org/pub/linux/kernel/v4.x/' \
#         --regex='linux-(__VER__)\.tar'
#
# check_release.pl --name=app --changelog=/usr/local/app/CHANGELOG \
#         --url='http://sourceforge.net/projects/app/files/app/' \
#         --regex='app/files/app/([^v]__VER__)/'

glob %ERRORS=( OK => 0, WARNING => 1, CRITICAL => 2, UNKNOWN => 3 );
glob %ERRCODES=( 0 => 'OK', 1 => 'WARNING', 2 => 'CRITICAL', 3 => 'UNKNOWN' );
glob $STATUS; glob $MSG;

# Error code if local version is newer than detected upstream version
my $warn_l_gt_r = $ERRORS{'WARNING'};
# Only download a new changelog once every day
glob $maxage = '1';
# Default to not consider pre-releases, betas, releas candidates, ...
glob $pre_releases = '0';
# List of strings to be matched against for detecting pre-releases
glob @pre_strings = qw(final rc RC pre prerelease preview test omega epsilon delta gamma beta alpha -a -b -c);

glob $name; glob $changelog; glob $url; glob $regex; glob $pre_releases; glob $proxy;

use File::Basename;
use Dpkg::Version;
use Getopt::Long;
require LWP::UserAgent;
require HTTP::Request;

my $help = 0;
my $getopt_result = GetOptions(
    "name=s"       => \$name,
    "changelog=s"  => \$changelog,
    "url=s"        => \$url,
    "regex=s"      => \$regex,
    "pre-releases" => \$pre_releases,
    "proxy=s"      => \$proxy,
    "help"         => \$help,
);
if ( not $getopt_result ) {
    $help = "help";
}

glob $urlfile = "/tmp/monitoring-check-release-$name.page";
if (!$name || !$changelog || !$url || !$regex || !$urlfile || !$maxage) {
    print "CRITICAL: Configuration error: Unset variable\n";
    exit $ERRORS{'CRITICAL'};
}

if ($help) {
    print qq{Usage: check_release.pl [options]

  --name=<name>              name of the application
  --changelog=<changelog>    path to local ChangeLog file
  --url=<url>                URL to upstream release page
  --regex=<regex>            regex used to parse upstream page for versions
  --pre-releases             consider pre-releases (-pre,-rc,-beta,...)
  --proxy=<proxy>            set proxy for ftp, http and https connections
  --help                     print this help and exit
};
    exit $ERRORS{'UNKNOWN'};
};

my $l_vers = get_local_version();
my $u_vers = get_upstream_version();

if (version_compare($l_vers, $u_vers) == "-1") {
    $STATUS=$ERRORS{'CRITICAL'};
    $MSG="$name: New release available: $u_vers, installed version: $l_vers (URL: $url)";
} elsif (version_compare($l_vers, $u_vers) == "0") {
    $STATUS=$ERRORS{'OK'};
    $MSG="$name: Newest release installed $u_vers";
} elsif (version_compare($l_vers, $u_vers) == "1") {
    if ($warn_l_gt_r) {
         $STATUS=$ERRORS{'WARNING'};
    } else {
         $STATUS=$ERRORS{'OK'};
    }
    $MSG="$name: Installed release newer ($l_vers) than available upstream ($u_vers)";
}

exit_code();

print "Installed version: $l_vers\n";
print "Available version: $u_vers\n";

sub get_local_version {
    my @diag = ();
    my @ch_vers = ();

    open(my $in, '<', $changelog) or die "Could not open ($changelog) File";
    while (<$in>) {
        chomp;
        if (/^(\d|v\d)/) {
        # Common
            my ($cvers, $date) = split(/\s+/, $_, 2);
            push (@ch_vers, "$cvers");
        } elsif (/^\s+version: ([\d.]+)$/) {
        # YAML
            push (@ch_vers, "$1");
        } elsif (/^\* (v?[\d._]+)$/) {
        # Apocal
            push (@ch_vers, "$1");
        } elsif (/^Version (v?\d[\S]+)($|[:,[:space:]])/i) {
        # Plain "Version N"
            push (@ch_vers, "$1");
        } elsif (/^Release (v?\d[\S]+)($|[:,[:space:]])/i) {
        # Plain "Release N"
            push (@ch_vers, "$1");
        } elsif (/^\s*Linux (v?\d[\d\._]+)($|[:,[:space:]])/i) {
        # Linux Kernel "Linux N"
            push (@ch_vers, "$1");
        } elsif (/^#+ $name (v?\d[\S]+)($|[:,[:space:]])/i) {
        # Plain "# <NAME> N"
            push (@ch_vers, "$1");
        } elsif (/^\$OC_VersionString = \'([\d.]+)\';$/) {
        # Nextcloud Version.php
            push (@ch_vers, "$1");
        } elsif (/^#\s([\d.]+)/) {
        # Plain "# 1.2.3.4" e.g. etherpad-lite CHANGELOG.md
            push (@ch_vers, "$1");
        }
    }
    close($in) or die "Could not close ($changelog) file";

    if (! @ch_vers) {
        $STATUS=$ERRORS{'CRITICAL'};
        $MSG="Unable to get version from changelog file ($changelog)";
    }

    # get most recent version from changelog
    my $vers_recent = '0';
    for my $vers (@ch_vers) {
        if (version_compare($vers, $vers_recent) == 1) {
            $vers_recent = $vers;
        }
    }

    if ($vers_recent eq '0') {
        $STATUS=$ERRORS{'CRITICAL'};
        $MSG="Unable to get version from changelog file ($changelog)";
    }

    return $vers_recent;
}

sub get_upstream_version {
    my @diag = ();
    my @up_vers = ();

    my $isfile = -e $urlfile;
    my $fileage = -M $urlfile;
    #print "urlfile age: $fileage\n";
    if (!$isfile || $fileage gt "$maxage") {
        #print "urlfile to old, fetching new\n";
        open(my $out, '>', $urlfile) or die "Could not open ($urlfile) File";
        print $out fetch_url();
        close($out) or die "Could not close ($urlfile) file";
    }

    # version parsing regex templates and suchlike
    my %sub_vers = ();
    my $no = 0;
    foreach (@pre_strings) {
        @sub_vers{split /\+/} = (--$no) x (@{[/\+/g]}+1)
    }
    my $re_ver = "(?i:[0-9._-]|(?<![a-z])(?:[a-z]|@{[join '|', keys %sub_vers]})(?![a-z]))+";
    $regex =~ s/__VER__/$re_ver/g;
    #print "regex: $regex\n";

    open(my $in, '<', $urlfile) or die "Could not open ($urlfile) File";
    while (<$in>) {
        chomp;
        if (/$regex/) {
            push (@url_vers, "$1");
        }
    }
    close($in) or die "Could not close ($urlfile) file";

    if (! @url_vers) {
        $STATUS=$ERRORS{'CRITICAL'};
        $MSG="Unable to get versions from release file ($urlfile)";
        exit_code();
    }

    # get most recent version from $url_data
    my $vers_recent = '0';
    my $pre_strings_combined = join("|",@pre_strings);
    for my $vers (@url_vers) {
        if ((not $pre_releases) && $vers =~ /($pre_strings_combined)/) {
            next;
        }
        if (version_compare($vers, $vers_recent) == 1) {
            $vers_recent = $vers;
        }
    }

    return $vers_recent;
}

sub fetch_url {
    my $xfersum = 0;  # bytes transferred
    my $ua = new LWP::UserAgent;
    $ua->agent("monitoring-plugin check_release");
    if ($proxy) {
        $ua->proxy(['ftp', 'http', 'https'] => $proxy);
    }
    my $req = HTTP::Request->new(GET => $url);
    local $_ = $ua->request($req);
    $xfersum += length($_->headers) + length $_->content;
    $_->content
}

sub exit_code {
    print "$ERRCODES{$STATUS}: ";
    print "$MSG";
    print "| STATUS=$STATUS\n";
    exit $STATUS;
}
